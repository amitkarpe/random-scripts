#!/bin/bash

wget https://releases.hashicorp.com/nomad/0.9.3/nomad_0.9.3_linux_amd64.zip

if [ -x `which unzip` ]
    unzip nomad_0.9.3_linux_amd64.zip
fi

sudo cp -iv nomad /usr/local/bin/

nomad --version