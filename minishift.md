
## Installing Minikube on Windows 10

#### REQUIREMENTS
Windows 10 Enterprise (or) Home Edition
Virtual Box

#### INSTALLING VIRTUAL BOX

https://docs.okd.io/latest/minishift/getting-started/quickstart.html
https://github.com/minishift/minishift/issues/1923

#### DOWNLOAD oc (OpenShift CLI) Client Tools

For [OpenShift Origin Client Tools] (https://blog.openshift.com/installing-oc-tools-windows/) can check [origin repository from github](https://github.com/openshift/origin/releases) for various release version. As of now on 6th Novermber the latest version for [windows is 3.11](https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-windows.zip) which can be downloaded either from github or [okd download](https://www.okd.io/download.html) page.

```shell
> curl -LO https://github.com/openshift/origin/releases/download/v3.11.0/openshift-origin-client-tools-v3.11.0-0cbc58b-windows.zip
  

```


#### DOWNLOAD MINIKUBE

```shell
> curl -LO 

```

cp c:\Users\akarpe\Downloads\__backup\software\kube\shift\* .

#### 

```shell
> ./kubectl.exe version
Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.0", GitCommit:"0ed33881dc4355495f623c6f22e7dd0b7632b7c0", GitTreeState:"clean", BuildDate:"2018-09-27T17:05:32Z", GoVersion:"go1.10.4", Compiler:"gc", Platform:"windows/amd64"}
Unable to connect to the server: dial tcp [::1]:8080: connectex: No connection could be made because the target machine actively refused it.

> ./minikube.exe version
minikube version: v0.30.0

cp c:\Users\akarpe\Downloads\__backup\software\kube\ssh.exe .

C:\WINDOWS\system32>cp c:\Users\akarpe\Downloads\__backup\software\kube\* .

C:\WINDOWS\system32>ls -lh mini*
-rwxrwxrwx  1 akarpe 0 41M 2018-11-05 11:32 minikube.exe

C:\WINDOWS\system32>ls -lh kube*
-rwxrwxrwx  1 akarpe 0 55M 2018-11-05 11:32 kubectl.exe

C:\WINDOWS\system32>

```

#### 

```shell

C:\WINDOWS\system32> minikube start --vm-driver=virtualbox


VBOX_MSI_INSTALL_PATH=C:\Program Files\Oracle\VirtualBox\


C:\Users\akarpe>minishift start --vm-driver=virtualbox
-- Starting profile 'minishift'
-- Check if deprecated options are used ... OK
-- Checking if https://github.com is reachable ... OK
-- Checking if requested OpenShift version 'v3.11.0' is valid ... OK
-- Checking if requested OpenShift version 'v3.11.0' is supported ... OK
-- Checking if requested hypervisor 'virtualbox' is supported on this platform ... OK
-- Checking if VirtualBox is installed ... OK
-- Checking the ISO URL ... OK
-- Downloading OpenShift binary 'oc' version 'v3.11.0'
 53.59 MiB / 53.59 MiB [===================================================================================] 100.00% 0s-- Downloading OpenShift v3.11.0 checksums ... OK
-- Checking if provided oc flags are supported ... OK
-- Starting the OpenShift cluster using 'virtualbox' hypervisor ...
-- Minishift VM will be configured with ...
   Memory:    4 GB
   vCPUs :    2
   Disk size: 20 GB

   Downloading ISO 'https://github.com/minishift/minishift-centos-iso/releases/download/v1.13.0/minishift-centos7.iso'
 52.11 MiB / 346.00 MiB [=======================>--------------------------------------------------------------------------------------------------------------------------------------]  15.06% 13m30s





C:\Users\akarpe>minishift oc-env
SET PATH=C:\Users\akarpe\.minishift\cache\oc\v3.11.0\windows;%PATH%
REM Run this command to configure your shell:
REM     @FOR /f "tokens=*" %i IN ('minishift oc-env') DO @call %i



minishift start --vm-driver virtualbox machine-logs --v=5




C:\Users\akarpe>minishift start --vm-driver=virtualbox
-- Starting profile 'minishift'
-- Check if deprecated options are used ... OK
-- Checking if https://github.com is reachable ... OK
-- Checking if requested OpenShift version 'v3.11.0' is valid ... OK
-- Checking if requested OpenShift version 'v3.11.0' is supported ... OK
-- Checking if requested hypervisor 'virtualbox' is supported on this platform ... OK
-- Checking if VirtualBox is installed ... OK
-- Checking the ISO URL ... OK
-- Downloading OpenShift binary 'oc' version 'v3.11.0'
 53.59 MiB / 53.59 MiB [===================================================================================] 100.00% 0s-- Downloading OpenShift v3.11.0 checksums ... OK
-- Checking if provided oc flags are supported ... OK
-- Starting the OpenShift cluster using 'virtualbox' hypervisor ...
-- Minishift VM will be configured with ...
   Memory:    4 GB
   vCPUs :    2
   Disk size: 20 GB

   Downloading ISO 'https://github.com/minishift/minishift-centos-iso/releases/download/v1.13.0/minishift-centos7.iso'
 346.00 MiB / 346.00 MiB [=================================================================================================================================================================] 100.00% 0s
-- Starting Minishift VM ............................................................. OK
-- Checking for IP address ... OK
-- Checking for nameservers ... OK
-- Checking if external host is reachable from the Minishift VM ...
   Pinging 8.8.8.8 ... OK
-- Checking HTTP connectivity from the VM ...
   Retrieving http://minishift.io/index.html ... FAIL
   VM cannot connect to external URL with HTTP
-- Checking if persistent storage volume is mounted ... OK
-- Checking available disk space ... 1% used OK
-- Writing current configuration for static assignment of IP address ... OK
   Importing 'openshift/origin-control-plane:v3.11.0' . CACHE MISS
   Importing 'openshift/origin-docker-registry:v3.11.0' . CACHE MISS
   Importing 'openshift/origin-haproxy-router:v3.11.0' . CACHE MISS
-- OpenShift cluster will be configured with ...
   Version: v3.11.0
-- Pulling the Openshift Container Image .......................... OK
-- Copying oc binary from the OpenShift container image to VM ... OK
-- Starting OpenShift cluster ......................................................................
Getting a Docker client ...
Checking if image openshift/origin-control-plane:v3.11.0 is available ...
Pulling image openshift/origin-cli:v3.11.0
E1105 05:42:25.126449    2450 helper.go:173] Reading docker config from /home/docker/.docker/config.json failed: open /home/docker/.docker/config.json: no such file or directory, will attempt to pull image docker.io/openshift/origin-cli:v3.11.0 anonymously
Image pull complete
E1105 05:42:34.416395    2450 helper.go:173] Reading docker config from /home/docker/.docker/config.json failed: open /home/docker/.docker/config.json: no such file or directory, will attempt to pull image docker.io/openshift/origin-node:v3.11.0 anonymously
Pulling image openshift/origin-node:v3.11.0
Pulled 5/6 layers, 85% complete
Pulled 6/6 layers, 100% complete
Extracting
Image pull complete
Checking type of volume mount ...
Determining server IP ...
Using public hostname IP 192.168.99.100 as the host IP
Checking if OpenShift is already running ...
Checking for supported Docker version (=>1.22) ...
Checking if insecured registry is configured properly in Docker ...
Checking if required ports are available ...
Checking if OpenShift client is configured properly ...
Checking if image openshift/origin-control-plane:v3.11.0 is available ...
Starting OpenShift using openshift/origin-control-plane:v3.11.0 ...
I1105 05:43:04.385392    2450 config.go:40] Running "create-master-config"
I1105 05:43:07.433109    2450 config.go:46] Running "create-node-config"
I1105 05:43:08.268223    2450 flags.go:30] Running "create-kubelet-flags"
I1105 05:43:08.639975    2450 run_kubelet.go:49] Running "start-kubelet"
I1105 05:43:08.832047    2450 run_self_hosted.go:181] Waiting for the kube-apiserver to be ready ...
I1105 05:44:43.870683    2450 interface.go:26] Installing "kube-proxy" ...
I1105 05:44:43.870723    2450 interface.go:26] Installing "kube-dns" ...
I1105 05:44:43.870730    2450 interface.go:26] Installing "openshift-service-cert-signer-operator" ...
I1105 05:44:43.870737    2450 interface.go:26] Installing "openshift-apiserver" ...
I1105 05:44:43.870767    2450 apply_template.go:81] Installing "openshift-apiserver"
I1105 05:44:43.871231    2450 apply_template.go:81] Installing "kube-proxy"
I1105 05:44:43.872445    2450 apply_template.go:81] Installing "kube-dns"
I1105 05:44:43.875541    2450 apply_template.go:81] Installing "openshift-service-cert-signer-operator"
I1105 05:44:51.022888    2450 interface.go:41] Finished installing "kube-proxy" "kube-dns" "openshift-service-cert-signer-operator" "openshift-apiserver"
I1105 05:46:51.057488    2450 run_self_hosted.go:242] openshift-apiserver available
I1105 05:46:51.058136    2450 interface.go:26] Installing "openshift-controller-manager" ...
I1105 05:46:51.058174    2450 apply_template.go:81] Installing "openshift-controller-manager"
I1105 05:46:54.368644    2450 interface.go:41] Finished installing "openshift-controller-manager"
Adding default OAuthClient redirect URIs ...
Adding persistent-volumes ...
Adding web-console ...
Adding centos-imagestreams ...
Adding registry ...
Adding router ...
Adding sample-templates ...
I1105 05:46:54.397615    2450 interface.go:26] Installing "persistent-volumes" ...
I1105 05:46:54.397627    2450 interface.go:26] Installing "openshift-web-console-operator" ...
I1105 05:46:54.397636    2450 interface.go:26] Installing "centos-imagestreams" ...
I1105 05:46:54.397641    2450 interface.go:26] Installing "openshift-image-registry" ...
I1105 05:46:54.397646    2450 interface.go:26] Installing "openshift-router" ...
I1105 05:46:54.397655    2450 interface.go:26] Installing "sample-templates" ...
I1105 05:46:54.397699    2450 interface.go:26] Installing "sample-templates/postgresql" ...
I1105 05:46:54.397705    2450 interface.go:26] Installing "sample-templates/cakephp quickstart" ...
I1105 05:46:54.397710    2450 interface.go:26] Installing "sample-templates/django quickstart" ...
I1105 05:46:54.397791    2450 interface.go:26] Installing "sample-templates/nodejs quickstart" ...
I1105 05:46:54.397801    2450 interface.go:26] Installing "sample-templates/jenkins pipeline ephemeral" ...
I1105 05:46:54.397809    2450 interface.go:26] Installing "sample-templates/mongodb" ...
I1105 05:46:54.397814    2450 interface.go:26] Installing "sample-templates/mariadb" ...
I1105 05:46:54.397818    2450 interface.go:26] Installing "sample-templates/mysql" ...
I1105 05:46:54.397824    2450 interface.go:26] Installing "sample-templates/dancer quickstart" ...
I1105 05:46:54.397830    2450 interface.go:26] Installing "sample-templates/rails quickstart" ...
I1105 05:46:54.397835    2450 interface.go:26] Installing "sample-templates/sample pipeline" ...
I1105 05:46:54.397883    2450 apply_list.go:67] Installing "sample-templates/sample pipeline"
I1105 05:46:54.398659    2450 apply_template.go:81] Installing "openshift-web-console-operator"
I1105 05:46:54.398873    2450 apply_list.go:67] Installing "centos-imagestreams"
I1105 05:46:54.399558    2450 apply_list.go:67] Installing "sample-templates/postgresql"
I1105 05:46:54.399707    2450 apply_list.go:67] Installing "sample-templates/cakephp quickstart"
I1105 05:46:54.399829    2450 apply_list.go:67] Installing "sample-templates/django quickstart"
I1105 05:46:54.399924    2450 apply_list.go:67] Installing "sample-templates/nodejs quickstart"
I1105 05:46:54.400008    2450 apply_list.go:67] Installing "sample-templates/jenkins pipeline ephemeral"
I1105 05:46:54.400081    2450 apply_list.go:67] Installing "sample-templates/mongodb"
I1105 05:46:54.400157    2450 apply_list.go:67] Installing "sample-templates/mariadb"
I1105 05:46:54.400265    2450 apply_list.go:67] Installing "sample-templates/mysql"
I1105 05:46:54.400363    2450 apply_list.go:67] Installing "sample-templates/dancer quickstart"
I1105 05:46:54.400507    2450 apply_list.go:67] Installing "sample-templates/rails quickstart"
I1105 05:47:09.993371    2450 interface.go:41] Finished installing "sample-templates/postgresql" "sample-templates/cakephp quickstart" "sample-templates/django quickstart" "sample-templates/nodejs quickstart" "sample-templates/jenkins pipeline ephemeral" "sample-templates/mongodb" "sample-templates/mariadb" "sample-templates/mysql" "sample-templates/dancer quickstart" "sample-templates/rails quickstart" "sample-templates/sample pipeline"
I1105 05:48:11.360547    2450 interface.go:41] Finished installing "persistent-volumes" "openshift-web-console-operator" "centos-imagestreams" "openshift-image-registry" "openshift-router" "sample-templates"
Login to server ...
Creating initial project "myproject" ...
Server Information ...
OpenShift server started.

The server is accessible via web console at:
    https://192.168.99.100:8443/console

You are logged in as:
    User:     developer
    Password: <any value>

To login as administrator:
    oc login -u system:admin


-- Exporting of OpenShift images is occuring in background process with pid 49060.




C:\Users\akarpe>oc login -u system:admin
Logged into "https://192.168.99.100:8443" as "system:admin" using existing credentials.

You have access to the following projects and can switch between them with 'oc project <projectname>':

    default
    kube-dns
    kube-proxy
    kube-public
    kube-system
  * myproject
    openshift
    openshift-apiserver
    openshift-controller-manager
    openshift-core-operators
    openshift-infra
    openshift-node
    openshift-service-cert-signer
    openshift-web-console

Using project "myproject".


C:\Users\akarpe>oc get nodes
NAME        STATUS    ROLES     AGE       VERSION
localhost   Ready     <none>    34m       v1.11.0+d4cacc0
