cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF


cat <<EOF >>  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

systemctl disable firewalld && systemctl stop firewalld
sestatus
getenforce
setenforce 0
echo "SELINUX=disabled" > /etc/sysconfig/selinux
swapoff -a
sed -e '/swap/ s/^#*/#/' -i /etc/fstab

sysctl --system
#yum install -y kubelet kubeadm kubectl docker
yum install -y docker-1.13.1
yum install -y kubelet-1.8.10 kubeadm-1.8.10 kubectl-1.8.10 kubernetes-cni-0.5.1-1.x86_64
systemctl enable kubelet && systemctl start kubelet
systemctl enable docker && systemctl start docker