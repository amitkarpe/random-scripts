#!/bin/bash

# Updates ubuntu
sudo apt update
sudo apt upgrade -y
# Installs packages necessary to run go
sudo apt install build-essential -y
# Installs go
wget https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz
sudo tar -xvf go1.12.7.linux-amd64.tar.gz
sudo mv go /usr/local
# Updates environmental variables to include go
cat <<EOF>> ~/.profile
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
EOF
source ~/.profile

go version
# Should return go version go1.12.7 linux/amd64

git clone https://github.com/Kava-Labs/kava.git
cd kava
git checkout v0.2.0
make install

kvd version --long
# name: kava
# servername: kvd
# clientname: kvcli
# version: 0.2.0
# gitcommit: 35829b9f253814dd1ea5d5d2cba306fb9aab14b3
# buildtags: netgo,ledger
# goversion: go version go1.12.7 darwin/amd64


# Replace <your-moniker> with the publicly viewable name you want for your validator.
kvd init --chain-id kava-testnet-2000 SWS
# Create a wallet for your node. <your-wallet-name> is just a human readable name you can use to remember your wallet. It can be the same or different than your moniker.
kvcli keys add SWS --recover


kvd add-genesis-account $(kvcli keys show SWS -a) 5000000000000ukava

kvd gentx --name SWS  --amount 5000000000000ukava --ip 148.251.18.178



