

While correcting total token ammount from ```1000000000000ukava``` to ```5000000000000ukava```, received following error:

```
kava2@hz-eu-central-1-staging:~$ kvd add-genesis-account $(kvcli keys show SWS -a) 5000000000000ukava
ERROR: the application state already contains account 35E7631485750E4FA16822C6640BEA34311E1FE8
```

Tried to unsafe-reset-all but that did not reset "account details".

```
kava2@hz-eu-central-1-staging:~$ kvd unsafe-reset-all
I[2019-07-30|11:03:36.789] Removed all blockchain history               module=main dir=/opt/validation/kava2/.kvd/data
I[2019-07-30|11:03:36.799] Reset private validator file to genesis state module=main keyFile=/opt/validation/kava2/.kvd/config/priv_validator_key.json stateFile=/opt/validation/kava2/.kvd/data/priv_validator_state.json

kava2@hz-eu-central-1-staging:~$ kvd add-genesis-account $(kvcli keys show SWS -a) 5000000000000ukava
ERROR: the application state already contains account 35E7631485750E4FA16822C6640BEA34311E1FE8
```

Manually deleted account entries from ```.kvd/config/genesis.json```.

```
kava2@hz-eu-central-1-staging:~$ vim  .kvd/config/genesis.json
kava2@hz-eu-central-1-staging:~$ kvd add-genesis-account $(kvcli keys show SWS -a) 5000000000000ukava
```

Signed a gentx that creates your validator in the genesis file.

```
kava2@hz-eu-central-1-staging:~$ kvd gentx --name SWS  --amount 5000000000000ukava --ip 148.251.18.178
Password to sign with 'SWS':
ERROR: open /opt/validation/kava2/.kvd/config/gentx/gentx-3b477dfe9a530352bb0edaaaa1ca08129792ea04.json: file exists
kava2@hz-eu-central-1-staging:~$ mv /opt/validation/kava2/.kvd/config/gentx/gentx-3b477dfe9a530352bb0edaaaa1ca08129792ea04.json  gentx-3b477dfe9a530352bb0edaaaa1ca08129792ea04.json.bak
kava2@hz-eu-central-1-staging:~$ kvd gentx --name SWS  --amount 5000000000000ukava --ip 148.251.18.178
Password to sign with 'SWS':
Genesis transaction written to "/opt/validation/kava2/.kvd/config/gentx/gentx-3b477dfe9a530352bb0edaaaa1ca08129792ea04.json"
```
