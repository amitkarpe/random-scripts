#!/bin/bash

moniker=$1
ip=$2

# Updates ubuntu
sudo apt update
sudo apt upgrade -y
# Installs packages necessary to run go
sudo apt install build-essential wget git vim tmux jq  -y
# Installs go
wget https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz
sudo tar -xvf go1.12.7.linux-amd64.tar.gz
sudo mv go /usr/local
# Updates environmental variables to include go
cat <<EOF>> ~/.profile
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
EOF
source ~/.profile

go version
# Should return go version go1.12.7 linux/amd64

git clone https://github.com/Kava-Labs/kava.git
cd kava
git checkout v0.2.0
make install

kvd version --long
# name: kava
# servername: kvd
# clientname: kvcli
# version: 0.2.0
# gitcommit: 35829b9f253814dd1ea5d5d2cba306fb9aab14b3
# buildtags: netgo,ledger
# goversion: go version go1.12.7 darwin/amd64


# Replace <your-moniker> with the publicly viewable name you want for your validator.
kvd init --chain-id kava-testnet-2000 ${moniker}
# Create a wallet for your node. <your-wallet-name> is just a human readable name you can use to remember your wallet. It can be the same or different than your moniker.
kvcli keys add ${moniker}
kvd add-genesis-account $(kvcli keys show ${moniker} -a) 5000000000000ukava
kvd gentx --name ${moniker}  --amount 5000000000000ukava --ip ${ip}
mv ~/.kvd/config/genesis.json ~/.kvd/config/orig.genesis.json
wget https://raw.githubusercontent.com/Kava-Labs/kava-testnets/master/2000/genesis.json -P ~/.kvd/config
ls -lh ~/.kvd/config

sudo mkdir -p /var/log/kvd && sudo touch /var/log/kvd/kvd.log && sudo touch /var/log/kvd/kvd_error.log

sudo tee /etc/systemd/system/kvd.service > /dev/null <<'EOF'
[Unit]
Description=Kava daemon
After=network-online.target

[Service]
User=kava
ExecStart=/opt/validation/kava/go/bin/kvd start
StandardOutput=file:/var/log/kvd/kvd.log
StandardError=file:/var/log/kvd/kvd_error.log
Restart=always
RestartSec=3
LimitNOFILE=4096

[Install]
WantedBy=multi-user.target
EOF
# Start the node
sudo systemctl enable --now kvd
sudo journalctl -f -u kvd