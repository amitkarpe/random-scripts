CheckList

- is NodeId correct?
`kvd tendermint show-node-id` should equal to `.memo` in the gentx.json

3b477dfe9a530352bb0edaaaa1ca08129792ea04@148.251.18.178:26656
Format:
[NodeId]@[SERVER_IP]:[PORT]

```
kava2@hz-eu-central-1-staging:~$ kvd tendermint show-node-id
3b477dfe9a530352bb0edaaaa1ca08129792ea04

kava2@hz-eu-central-1-staging:~$ cat /tmp/gentx2.json | jq .value.memo
"3b477dfe9a530352bb0edaaaa1ca08129792ea04@148.251.18.178:26656"
```


- is PublicKey correct?

`kvd tendermint show-validator` should equal to `.value.msg[0].pubkey`

e.g
kavavalconspub1zcjduepq5rkwxqt22qxj50g7sx0p2wczt30v8ts4rgjc6f2yurk077ykkr0spr3r0s

```
kava2@hz-eu-central-1-staging:~$ kvd tendermint show-validator
kavavalconspub1zcjduepq5rkwxqt22qxj50g7sx0p2wczt30v8ts4rgjc6f2yurk077ykkr0spr3r0s

kava2@hz-eu-central-1-staging:~$ cat /tmp/gentx2.json | jq .value.msg[].value.pubkey
"kavavalconspub1zcjduepq5rkwxqt22qxj50g7sx0p2wczt30v8ts4rgjc6f2yurk077ykkr0spr3r0s"
```

- is Delegator Address correct?

`kvcli keys list` should equal to `.value.msg[0].delegator_address`

e.g
NAME:   TYPE:   ADDRESS:                                        PUBKEY:
SWS     local   kava1xhnkx9y9w58ylgtgytrxgzl2xsc3u8lg7dka6j     kavapub1addwnpepq2zcmpu3cd3tm8anrqe9gm5u2zwz2t0exkrf0n4qxzqguzj7r4tmuvhvfxa

```
kava2@hz-eu-central-1-staging:~$ kvcli keys list
NAME:	TYPE:	ADDRESS:					PUBKEY:
SWS	local	kava1xhnkx9y9w58ylgtgytrxgzl2xsc3u8lg7dka6j	kavapub1addwnpepq2zcmpu3cd3tm8anrqe9gm5u2zwz2t0exkrf0n4qxzqguzj7r4tmuvhvfxa

kava2@hz-eu-central-1-staging:~$ cat /tmp/gentx2.json | jq .value.msg[].value.validator_address
"kavavaloper1xhnkx9y9w58ylgtgytrxgzl2xsc3u8lgnmv4z9"
kava2@hz-eu-central-1-staging:~$ cat /tmp/gentx2.json | jq .value.msg[].value.delegator_address
"kava1xhnkx9y9w58ylgtgytrxgzl2xsc3u8lg7dka6j"

```