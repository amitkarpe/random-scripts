#!/bin/bash

user=$1
echo Going to add user "${user}"
mkdir -p /opt/validation/
sudo useradd ${user}  -d /opt/validation/${user} -s /bin/bash -m
echo Going to create password for "${user}"
sudo passwd ${user}
sudo usermod -aG sudo ${user}
sudo usermod -aG admin ${user}
sudo usermod -aG wheel ${user}
echo "done"
