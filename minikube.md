
## Installing Minikube on Windows 10

#### REQUIREMENTS
Windows 10 Enterprise (or) Home Edition
Virtual Box

#### INSTALLING VIRTUAL BOX

#### DOWNLOAD KUBECTL
https://github.com/kubernetes/minikube/releases
https://github.com/kubernetes/minikube/releases/tag/v0.30.0


```shell
 $ curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.12.0/bin/windows/amd64/kubectl.exe
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 54.9M  100 54.9M    0     0   258k      0  0:03:38  0:03:38 --:--:--  214k

```


#### DOWNLOAD MINIKUBE

```shell
> curl -LO https://storage.googleapis.com/minikube/releases/v0.30.0/minikube-windows-amd64.exe

```

#### 

```shell
> ./kubectl.exe version
Client Version: version.Info{Major:"1", Minor:"12", GitVersion:"v1.12.0", GitCommit:"0ed33881dc4355495f623c6f22e7dd0b7632b7c0", GitTreeState:"clean", BuildDate:"2018-09-27T17:05:32Z", GoVersion:"go1.10.4", Compiler:"gc", Platform:"windows/amd64"}
Unable to connect to the server: dial tcp [::1]:8080: connectex: No connection could be made because the target machine actively refused it.

> ./minikube.exe version
minikube version: v0.30.0


C:\WINDOWS\system32>cp c:\Users\akarpe\Downloads\__backup\software\kube\* .

C:\WINDOWS\system32>ls -lh mini*
-rwxrwxrwx  1 akarpe 0 41M 2018-11-05 11:32 minikube.exe

C:\WINDOWS\system32>ls -lh kube*
-rwxrwxrwx  1 akarpe 0 55M 2018-11-05 11:32 kubectl.exe

C:\WINDOWS\system32>

```

#### 

```shell

C:\WINDOWS\system32> minikube start --vm-driver=virtualbox
Starting local Kubernetes v1.10.0 cluster...
Starting VM...
Downloading Minikube ISO
 170.78 MB / 170.78 MB  100.00% 0s
Getting VM IP address...
Moving files into cluster...
Downloading kubelet v1.10.0
Downloading kubeadm v1.10.0
Finished Downloading kubelet v1.10.0
Finished Downloading kubeadm v1.10.0
Setting up certs...
Connecting to cluster...
Setting up kubeconfig...
Starting cluster components...
Kubectl is now configured to use the cluster.
Loading cached images from config file.

```
#### 


```shell
 C:\WINDOWS\system32> kubectl get pods --all-namespaces=true
NAMESPACE     NAME                                    READY   STATUS    RESTARTS   AGE
kube-system   coredns-c4cffd6dc-qcj6v                 1/1     Running   0          2m
kube-system   etcd-minikube                           1/1     Running   0          1m
kube-system   kube-addon-manager-minikube             1/1     Running   0          1m
kube-system   kube-apiserver-minikube                 1/1     Running   0          1m
kube-system   kube-controller-manager-minikube        1/1     Running   0          1m
kube-system   kube-dns-86f4d74b45-fg22c               3/3     Running   0          2m
kube-system   kube-proxy-8xcv7                        1/1     Running   0          2m
kube-system   kube-scheduler-minikube                 1/1     Running   0          2m
kube-system   kubernetes-dashboard-6f4cfc5d87-5trpg   1/1     Running   0          2m
kube-system   storage-provisioner                     1/1     Running   0          2m

C:\WINDOWS\system32> kubectl get nodes
NAME       STATUS   ROLES    AGE   VERSION
minikube   Ready    master   2m    v1.10.0

```


Source:
https://www.assistanz.com/installing-minikube-on-windows-10-home-edition-using-virtualbox/
https://www.marksei.com/minikube-kubernetes-windows/