#!/bin/bash

export CLUSTER=$1;

/usr/local/bin/oc login -u admin -p 1 https://${CLUSTER}:8443 --insecure-skip-tls-verify=true > /dev/null
echo "------------------------------ ${CLUSTER} ------------------------------" |  tee /tmp/${CLUSTER}
echo "" |  tee -a /tmp/${CLUSTER}
/usr/local/bin/oc status | head -2 | tee -a /tmp/${CLUSTER}
/usr/local/bin/oc get nodes | tee -a /tmp/${CLUSTER}
echo "-" |  tee -a /tmp/${CLUSTER}
echo "------------------------------------------------------------" |  tee -a /tmp/${CLUSTER}
echo "-" |  tee -a /tmp/${CLUSTER}
echo "-" |  tee -a /tmp/${CLUSTER}


exit

Posted on https://portal.sevone.com/display/~akarpe/OpenShift+system+setup+in+lab
Hosted on http://akarpe-ubuntu6.sevone.com/index.html
Code is on openshift3

# Cron Job
# [root@openshift3 ~]# crontab -l


*/10 * * * * /root/oc_status.sh openshift1
0 * * * * /root/oc_status.sh openshift3
0 * * * * /root/oc_status.sh openshift5
0 * * * * /root/oc_status.sh openshift7

# Cron Job on WebServer - http://akarpe-ubuntu6.sevone.com/index.html
# root@akarpe-ubuntu6:/var/www/html# crontab -l

*/1 * * * * scp openshift3:/tmp/openshift? /var/www/html/; echo "" > /var/www/html/all; cat /var/www/html/openshift? >> /var/www/html/all; txt2html /var/www/html/all > /var/www/html/index.html
 