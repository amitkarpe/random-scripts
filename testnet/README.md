This steps should provide a generic guide to deploying a test net.

This guide also covers a sample test net deployment like kava etc. Where instructions should be using the specific deployment of test net with specific server settings. The object of this guide is to provide a high-level understanding of test net deployment procedure and to give an overview of the sequence of commands but it is always advisable to check project's latest instructions to avoid any non-trivial mistakes.

 There are following steps involves if a user wants to configure a server and getting started as a validator on any test net.

Install and setup the service
Create a Wallet
Create a Genesis Transaction
Submit Genesis Transaction

As an example, we are going to setup up Kava Test Net 2, for which I have followed instructions from the blog article.

Please review the pre-requisite for the server.
Ubuntu 18.04 OS ( Any Linux OS should work)
2 CPUs
4GB RAM
24GB SSD
Allow incoming connections on ports 26656
Static IP address (Elastic IP for AWS, floating IP for DigitalOcean, etc)



Warning: If you are going to deploy multiple projects on the same server then it is recommended that to create a new user with appropriate home directory. So you will avoid any negative effect on other projects.

sudo useradd <User name> -d <User home directory> -s <Shell path> -m
sudo useradd kava1  -d /opt/validation/kava1 -s /bin/bash -m

We’ll install security updates and the required packages to run Kava:

# Updates ubuntu
sudo apt update
sudo apt upgrade -y
# Installs packages necessary to run go
sudo apt install build-essential -y
# Installs go
wget https://dl.google.com/go/go1.12.5.linux-amd64.tar.gz
sudo tar -xvf go1.12.5.linux-amd64.tar.gz
sudo mv go /usr/local
# Updates environmental variables to include go
cat <<EOF>> ~/.profile
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export GO111MODULE=on
export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin
EOF
source ~/.profile

To verify that go is installed:
go version
# Should return go version go1.12.5 linux/amd64


Install the Kava daemon and cli
We’ll install the software needed to run the Kava blockchain. First, create your own fork so that you can submit a genesis transaction pull request if necessary. Head over to GitHub and click “Fork.”

# git clone git@github.com:<YOUR-USERNAME>/kava.git
git clone git@github.com:amitkarpe/kava.git
cd kava
git checkout v0.2.0
# Ensure GO Modules are enabled
export GO111MODULE=on
# install executables
make install
# install will build the binaries and copy it in the "$HOME/go/bin" directory.
$ echo $HOME/go/bin
/opt/validation/kava2/go/bin
$ ls -lh $HOME/go/bin
total 65M
-rwxrwxr-x 1 kava2 kava2 30M Aug  2 10:12 kvcli
-rwxrwxr-x 1 kava2 kava2 35M Aug  2 10:12 kvd

To verify the version

$ ~/go/bin/kvd version --long
name: kava
servername: kvd
clientname: kvcli
version: 0.2.0
gitcommit: 35829b9f253814dd1ea5d5d2cba306fb9aab14b3
buildtags: netgo,ledger
goversion: go version go1.12.7 linux/amd64

$ ~/go/bin/kvcli version --long
name: kava
servername: kvd
clientname: kvcli
version: 0.2.0
gitcommit: 35829b9f253814dd1ea5d5d2cba306fb9aab14b3
buildtags: netgo,ledger
goversion: go version go1.12.7 linux/amd64


Note: Make sure server have the latest Kava Service on the server.  If the version is older than the expected version, then please download it from GitHub project repository and install it.

We’ll setup the kvd software to run the current Kava testnet:
Create your validator

# Replace <your-moniker> with the publicly viewable name you want for your validator.
kvd init --chain-id kava-testnet-2000 <your-moniker>
e.g. 
kvd init --chain-id kava-testnet-2000 SWS

# Create a wallet for your node. <your-wallet-name> is just a human readable name you can use to remember your wallet. It can be the same or different than your moniker.
kvcli keys add <your_wallet_name>
e.g.
kvcli keys add SWS --recover

Tip: This will spit out your recovery mnemonic. Be sure to back up your mnemonic before proceeding to the next step!

Submit a genesis transaction

# Create an account with 1000000 kava tokens
kvd add-genesis-account $(kvcli keys show <your_wallet_name> -a) 1000000000000ukava
e.g. 
kvd add-genesis-account $(kvcli keys show SWS -a) 5000000000000ukava

# Sign a gentx that creates your validator in the genesis file. Note to pass your public ip to the --ip flag. 
kvd gentx --name <your_wallet_name> --amount 1000000000000ukava --ip <your-public-ip>
e.g.
kvd gentx --name SWS  --amount 5000000000000ukava --ip  xxx.xxx.xxx.xxx

This will write your genesis transaction to $HOME/.kvd/config/gentx/gentx-<gen-tx-hash>.json.

To submit the gentx you created, fork the kava-testnets repo.

# Be sure you forked the repo at https://github.com/kava-labs/kava-testnets under your user name first.
cd $HOME
git clone git@github.com:<YOUR-USERNAME>/kava-testnets.git
cd kava-testnets

cp $HOME/.kvd/config/gentx/* $HOME/kava-testnets/2000/.
git push


Create a pull request for <github-username>/kava-testnets:master against the master branch of the Kava testnets repo.

