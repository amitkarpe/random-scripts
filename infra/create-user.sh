# On Client / User side

#create public key
ssh-keygen -t rsa -b 4096 -C “dntan@stakewith.us”
cat .ssh/id_rsa.pub



# On Server side

# create user 
adduser -G docker -m  dntan
mkdir .ssh
chmod 700 .ssh

# copy it into ".ssh/authorized_keys"

cat << EOF >> .ssh/authorized_keys
ssh-rsa AAAAB3NzaCXXXXXxxxxxxxxxxxx
xxxxxxx
xxxxxxxx
dntan@stakewith.us
EOF



