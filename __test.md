---
presentation:
  width: 800
  height: 600
---

<!-- slide -->

### 1

### 2

**bold**

~~task~~

task

| 1   | 2   |
| --- | --- |
| a   | b   |




```bash {cmd=true}
ls .
```


```gnuplot {cmd=true output="html"}
set terminal svg
set title "Simple Plots" font ",20"
set key left box
set samples 50
set style data points

plot [-10:10] sin(x),atan(x),cos(atan(x))
```
<!-- slide -->

```python {cmd="/usr/local/bin/python3"}
    print("This will run python3 program")
```
<!-- slide -->

```shell-session


akarpe@akarpe-new-pc MINGW64 ~/github/del1 (master)

$ ls /cmd/ -lh

total 384K

-rwxr-xr-x 2 akarpe 197121  39K Oct  4 21:35 git.exe*

-rwxr-xr-x 1 akarpe 197121 145K Oct  4 21:35 git-gui.exe*

-rwxr-xr-x 1 akarpe 197121 145K Oct  4 21:35 gitk.exe*

-rwxr-xr-x 2 akarpe 197121  39K Oct  4 21:35 git-lfs.exe*

-rw-r--r-- 1 akarpe 197121 3.0K Oct  4 21:35 start-ssh-agent.cmd

-rw-r--r-- 1 akarpe 197121 2.7K Oct  4 21:35 start-ssh-pageant.cmd



akarpe@akarpe-new-pc MINGW64 ~/github/del1 (master)

$ git version

git version 2.19.1.windows.1



akarpe@akarpe-new-pc MINGW64 ~/github/del1 (master)



```

```python
print "foo";





```

echo "hello"

```js


akarpe@akarpe-new-pc MINGW64 ~/github/del1 (master)

$ ls /cmd/ -lh

total 384K

-rwxr-xr-x 2 akarpe 197121  39K Oct  4 21:35 git.exe*

-rwxr-xr-x 1 akarpe 197121 145K Oct  4 21:35 git-gui.exe*

-rwxr-xr-x 1 akarpe 197121 145K Oct  4 21:35 gitk.exe*

-rwxr-xr-x 2 akarpe 197121  39K Oct  4 21:35 git-lfs.exe*

-rw-r--r-- 1 akarpe 197121 3.0K Oct  4 21:35 start-ssh-agent.cmd

-rw-r--r-- 1 akarpe 197121 2.7K Oct  4 21:35 start-ssh-pageant.cmd



akarpe@akarpe-new-pc MINGW64 ~/github/del1 (master)

$ git version

git version 2.19.1.windows.1



akarpe@akarpe-new-pc MINGW64 ~/github/del1 (master)



```


